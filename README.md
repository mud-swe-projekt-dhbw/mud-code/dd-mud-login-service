# dd-mud-login-service

Ein REST-Service um sich bei der Anwendung anzumelden.

## Technik
Basiert auf Spring Boot und wervendet Spring MVC Web-Technologien.

## Schnittstelle

POST (/login/sign-up): Übergeben wird Username und neues Passwort, das das Hash gespeichert wird.

Anworten:
* 200: OK
* 400: z.B. "der Nutzername exsistiert schon" | "Passwort nicht ausreichend"

POST (/login/check-credentials): Übergebe ein Username und das Passwort

Anworten:
* 200: OK
* 401: Nutzername exsistiert nicht
* 403: Verboten