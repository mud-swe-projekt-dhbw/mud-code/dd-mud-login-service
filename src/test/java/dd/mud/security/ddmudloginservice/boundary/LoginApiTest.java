package dd.mud.security.ddmudloginservice.boundary;

import dd.mud.security.ddmudloginservice.boundary.model.PlayerInformationTO;
import dd.mud.security.ddmudloginservice.boundary.model.SignInTO;
import dd.mud.security.ddmudloginservice.control.aggregator.CheckCredentialsAggregator;
import dd.mud.security.ddmudloginservice.control.aggregator.NewCredentialsAggregator;
import dd.mud.security.ddmudloginservice.control.aggregator.PasswordForgottenAggregator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


class LoginApiTest {

    @InjectMocks
    LoginApi loginApi;

    @Mock
    private NewCredentialsAggregator newCredentialsAggregator;
    @Mock
    private CheckCredentialsAggregator checkCredentialsAggregator;
    @Mock
    private PasswordForgottenAggregator passwordForgottenAggregator;


    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void checkCredentialsTest() {
        SignInTO signInTO = new SignInTO("sebastian@duerr-edv.de", "1234");

        PlayerInformationTO playerInformationTO = new PlayerInformationTO(10L, "username",1L);
        when(checkCredentialsAggregator.checkCredentials(signInTO)).thenReturn(playerInformationTO);

        ResponseEntity<PlayerInformationTO> actual = loginApi.checkCredentials(signInTO);
        assertEquals(playerInformationTO, actual.getBody());
        assertEquals(HttpStatus.OK, actual.getStatusCode());
    }
}