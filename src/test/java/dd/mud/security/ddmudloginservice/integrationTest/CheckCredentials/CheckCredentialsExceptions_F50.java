package dd.mud.security.ddmudloginservice.integrationTest.CheckCredentials;

import com.google.gson.Gson;
import dd.mud.security.ddmudloginservice.boundary.model.SignInTO;
import dd.mud.security.ddmudloginservice.entity.model.User;
import dd.mud.security.ddmudloginservice.entity.repository.CredentialsRepository;
import org.hibernate.engine.spi.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CheckCredentialsExceptions_F50 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CredentialsRepository credentialsRepository;

    @Test
    public void checkCredentialsUsernameDoesNotExist() throws Exception {

        final String validPassword = "123";
        final String invalidUsername = "wrongUsername";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        true, 0L);

        Gson gson = new Gson();
        SignInTO signInTO = new SignInTO(invalidUsername, validPassword);

        when(credentialsRepository.findByUsername(invalidUsername))
                .thenReturn(Optional.empty());

        final MvcResult mvcResult = mockMvc.perform(post("/login/check-credentials")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(signInTO)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void checkCredentialsMailDoesNotExist() throws Exception {

        final String validPassword = "123";
        final String invalidMail = "wrongMail";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        true, 0L);

        Gson gson = new Gson();
        SignInTO signInTO = new SignInTO(invalidMail, validPassword);

        when(credentialsRepository.findByMail(invalidMail))
                .thenReturn(Optional.empty());

        final MvcResult mvcResult = mockMvc.perform(post("/login/check-credentials")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(signInTO)))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void checkCredentialsInvalidPassword() throws Exception {

        final String validPassword = "123";
        final String invalidPassword = "1234";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        true, 0L);

        Gson gson = new Gson();
        SignInTO signInTO = new SignInTO(validUserData.getMail(), invalidPassword);

        when(credentialsRepository.findByMail(validUserData.getMail()))
                .thenReturn(java.util.Optional.of(validUserData));

        final MvcResult mvcResult = mockMvc.perform(post("/login/check-credentials")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(signInTO)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void checkCredentialsUncompletedRegistration() throws Exception {

        final String validPassword = "123";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        true, 0L);
        validUserData.setRegistrationStatus(false);

        Gson gson = new Gson();
        SignInTO signInTO = new SignInTO(validUserData.getMail(), validPassword);

        when(credentialsRepository.findByMail(validUserData.getMail()))
                .thenReturn(java.util.Optional.of(validUserData));

        final MvcResult mvcResult = mockMvc.perform(post("/login/check-credentials")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(signInTO)))
                .andExpect(status().isLocked())
                .andReturn();
    }

    @Test
    public void checkCredentialsInternalServerError() throws Exception {

        final String validPassword = "123";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        true, 0L);

        Gson gson = new Gson();
        SignInTO signInTO = new SignInTO(validUserData.getMail(), validPassword);

        when(credentialsRepository.findByMail(validUserData.getMail()))
                .thenThrow(RuntimeException.class);

        final MvcResult mvcResult = mockMvc.perform(post("/login/check-credentials")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(signInTO)))
                .andExpect(status().isInternalServerError())
                .andReturn();
    }

}
