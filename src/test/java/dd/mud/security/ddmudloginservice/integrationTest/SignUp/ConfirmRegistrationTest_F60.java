package dd.mud.security.ddmudloginservice.integrationTest.SignUp;

import dd.mud.security.ddmudloginservice.entity.model.User;
import dd.mud.security.ddmudloginservice.entity.repository.CredentialsRepository;
import org.hibernate.engine.spi.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ConfirmRegistrationTest_F60 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CredentialsRepository credentialsRepository;

    @Test
    public void confirmRegistrationTest() throws Exception {
        String mailHash = "3434jh4k3kb434";
        String password = "newPassword";
        String mailAddress = "mueller.marcel69@gmail.com";

        User user = new User(1L,
                mailAddress,
                "elMolinero",
                "3434jh4k3kb434",
                "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4",
                true,
                0L);

        when(credentialsRepository.findByMailHash(mailHash)).thenReturn(Optional.of(user));
        when(credentialsRepository.save(user)).thenReturn(null);

        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/login/confirm-registration")
                .param("mailHash", mailHash))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void confirmRegistrationKeyNotFoundTest() throws Exception {
        String mailHash = "dfjdfjdöfk";

        User user = new User(1L,
                "mueller.marcel69@gmail.com",
                "elMolinero",
                mailHash,
                "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4",
                true,
                0L);

        when(credentialsRepository.findByMailHash(mailHash)).thenReturn(Optional.empty());
        when(credentialsRepository.save(user)).thenReturn(null);

        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/login/confirm-registration")
                .param("mailHash", mailHash))
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
