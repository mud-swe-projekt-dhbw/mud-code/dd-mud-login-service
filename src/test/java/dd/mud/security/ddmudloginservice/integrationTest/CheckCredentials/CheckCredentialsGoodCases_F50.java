package dd.mud.security.ddmudloginservice.integrationTest.CheckCredentials;

import com.google.gson.Gson;
import dd.mud.security.ddmudloginservice.boundary.model.PlayerInformationTO;
import dd.mud.security.ddmudloginservice.boundary.model.SignInTO;
import dd.mud.security.ddmudloginservice.entity.model.User;
import dd.mud.security.ddmudloginservice.entity.repository.CredentialsRepository;
import org.hibernate.engine.spi.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CheckCredentialsGoodCases_F50 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CredentialsRepository credentialsRepository;

    @Test
    public void checkCredentialsGoodCaseCallByMail() throws Exception {

        final String validPassword = "123";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        true, 0L);

        Gson gson = new Gson();
        SignInTO signInTO = new SignInTO(validUserData.getMail(), validPassword);
        PlayerInformationTO playerInformationTO =
                new PlayerInformationTO(validUserData.getId(), validUserData.getUsername(), validUserData.getRank());

        when(credentialsRepository.findByMail(validUserData.getMail())).thenReturn(Optional.of(validUserData));

        final MvcResult mvcResult = mockMvc.perform(post("/login/check-credentials")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(signInTO)))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(gson.toJson(playerInformationTO),
                mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void checkCredentialsGoodCaseCallByUsername() throws Exception {

        final String validPassword = "123";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        true, 0L);

        Gson gson = new Gson();
        SignInTO signInTO = new SignInTO(validUserData.getUsername(), validPassword);
        PlayerInformationTO playerInformationTO =
                new PlayerInformationTO(validUserData.getId(), validUserData.getUsername(), validUserData.getRank());

        when(credentialsRepository.findByUsername(validUserData.getUsername()))
                .thenReturn(java.util.Optional.of(validUserData));

        final MvcResult mvcResult = mockMvc.perform(post("/login/check-credentials")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(signInTO)))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(gson.toJson(playerInformationTO),
                mvcResult.getResponse().getContentAsString());
    }
}
