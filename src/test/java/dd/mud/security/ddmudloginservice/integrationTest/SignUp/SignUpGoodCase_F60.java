package dd.mud.security.ddmudloginservice.integrationTest.SignUp;

import com.google.gson.Gson;
import dd.mud.security.ddmudloginservice.boundary.model.CredentialsTO;
import dd.mud.security.ddmudloginservice.boundary.model.Mail;
import dd.mud.security.ddmudloginservice.control.service.MailService;
import dd.mud.security.ddmudloginservice.entity.model.User;
import dd.mud.security.ddmudloginservice.entity.repository.CredentialsRepository;
import org.hibernate.engine.spi.PersistenceContext;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SignUpGoodCase_F60 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CredentialsRepository credentialsRepository;

    @MockBean
    MailService mailService;

    @Test
    public void signUpGoodCase() throws Exception {

        final String validPassword = "123";
        User validUserData =
                new User(6L, "testa@integratione.de", "testa",
                        "686f635ae2cba67aed599f3954537be599dad37af3034f59fc9a3417359884b6",
                        "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3",
                        false, 0L);

        CredentialsTO credentialsTO = new CredentialsTO(validUserData.getMail(),
                validUserData.getUsername(), validPassword);

        Mail mail = new Mail("", "", "", "", "");

        Gson gson = new Gson();

        when(credentialsRepository.existsByUsername(credentialsTO.getUsername()))
                .thenReturn(false);
        when(credentialsRepository.existsByMail(credentialsTO.getMail()))
                .thenReturn(false);
        when(credentialsRepository.save(User.builder().build()))
                .thenReturn(null);

        when(mailService.buildConfirmationMail(credentialsTO.getMail(), credentialsTO.getMail()))
                .thenReturn(mail);
        when(mailService.isMailValid(credentialsTO.getMail()))
                .thenReturn(true);

        MvcResult mvcResult = mockMvc.perform(post("/login/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(credentialsTO)))
                .andExpect(status().isCreated())
                .andReturn();
    }

}
