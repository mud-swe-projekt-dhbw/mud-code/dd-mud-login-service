package dd.mud.security.ddmudloginservice.integrationTest.PasswordForgotten;

import com.google.gson.Gson;
import dd.mud.security.ddmudloginservice.boundary.LoginApi;
import dd.mud.security.ddmudloginservice.boundary.model.PasswordRequestTO;
import dd.mud.security.ddmudloginservice.entity.model.User;
import dd.mud.security.ddmudloginservice.entity.repository.CredentialsRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.engine.spi.PersistenceContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PasswordForgottenIntegrationTest_F70 {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    CredentialsRepository credentialsRepository;

    @Test
    public void sendPasswordForgottenMailTest() throws Exception{
        String mailAddress = "mueller.marcel69@gmail.com";
        User user = new User(1L,
                mailAddress,
                "elMolinero",
                "3434jh4k3kb434",
                "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4",
                true,
                0L);

        PasswordRequestTO passwordRequestTO = new PasswordRequestTO(mailAddress);
        Gson gson = new Gson();

        when(credentialsRepository.findByMail(mailAddress)).thenReturn(Optional.of(user));

        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/login/password-forgotten")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(passwordRequestTO)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void sendPasswordForgottenMailUserNotFoundTest() throws Exception{
        String mailAddress = "mueller.marcel69@gmail.com";
        User user = new User(1L,
                mailAddress,
                "elMolinero",
                "3434jh4k3kb434",
                "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4",
                true,
                0L);

        PasswordRequestTO passwordRequestTO = new PasswordRequestTO(mailAddress);
        Gson gson = new Gson();

        when(credentialsRepository.findByMail(mailAddress)).thenReturn(Optional.empty());

        final MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/login/password-forgotten")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(passwordRequestTO)))
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
