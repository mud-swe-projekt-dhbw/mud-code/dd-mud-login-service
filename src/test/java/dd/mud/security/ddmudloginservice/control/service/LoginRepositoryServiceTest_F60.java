package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.control.ForbiddenSymbolsUsername;
import dd.mud.security.ddmudloginservice.control.exceptions.UsernameSyntacticallyInvalidException;
import dd.mud.security.ddmudloginservice.entity.repository.CredentialsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertTrue;

class LoginRepositoryServiceTest_F60 {

    @InjectMocks
    LoginRepositoryService loginRepositoryService;

    @Mock
    CredentialsRepository credentialsRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isSyntaxValidUsingValidUsername() {
        String validUsername1 = "#Test123";
        String validUsername2 = "Test!";
        String validUsername3 = "5";
        String validUsername4 = "TestDm";

        assertTrue(loginRepositoryService.isSyntaxValid(validUsername1));
        assertTrue(loginRepositoryService.isSyntaxValid(validUsername2));
        assertTrue(loginRepositoryService.isSyntaxValid(validUsername3));
        assertTrue(loginRepositoryService.isSyntaxValid(validUsername4));
    }

    @Test
    public void isSyntaxValidUsingInvalidSymbols(){
        String invalidUsernameAt = "@Test";
        String invalidUsernameWhiteSpace = "Testina Testamente";
        String invalidUsernameColon = "Te:st";
        String invalidUsername = "@MyTest: Testable";

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(invalidUsernameAt);
        });

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(invalidUsernameWhiteSpace);
        });

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(invalidUsernameColon);
        });

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(invalidUsername);
        });
    }

    @Test
    public void isSyntaxValidUsingInvalidUsernames(){

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(ForbiddenSymbolsUsername.FORBIDDEN_USERNAME0);
        });

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(ForbiddenSymbolsUsername.FORBIDDEN_USERNAME1);
        });

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(ForbiddenSymbolsUsername.FORBIDDEN_USERNAME2);
        });

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(ForbiddenSymbolsUsername.FORBIDDEN_USERNAME3);
        });

        Assertions.assertThrows(UsernameSyntacticallyInvalidException.class, () -> {
            loginRepositoryService.isSyntaxValid(ForbiddenSymbolsUsername.FORBIDDEN_EMPTY_USERNAME);
        });
    }
}