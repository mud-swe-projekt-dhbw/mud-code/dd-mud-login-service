package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.boundary.model.PlayerInformationTO;
import dd.mud.security.ddmudloginservice.control.exceptions.PasswordIsWrongException;
import dd.mud.security.ddmudloginservice.entity.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class PasswordCheckingServiceTest_F50 {

    @InjectMocks
    CredentialsVerifierService credentialsVerifierService;

    @Mock
    LoginRepositoryService loginRepositoryService;

    @Mock
    HashingService hashingService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void checkPasswordWrongTest() {
        User user = new User(1L,
                "mueller.marcel69@gmail.com",
                "elMolinero",
                "3434jh4k3kb434",
                "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4",
                true,
                0L);

        String enteredPassword = "wrongPassword";

        PasswordCheckingService passwordCheckingService = new PasswordCheckingService(hashingService, loginRepositoryService);
        Assertions.assertThrows(PasswordIsWrongException.class, () -> {
            passwordCheckingService.checkPasswordAndReturnInformationTO(user, enteredPassword);
        });
    }

    @Test
    public void checkPasswordCorrectTest() {
        User user = new User(1L,
                "mueller.marcel69@gmail.com",
                "name",
                "3434jh4k3kb434",
                "03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4",
                true,
                0L);

        String correctPassword = "1234";

        PlayerInformationTO playerInformationTO = new PlayerInformationTO(1L, "name", 0L);

        when(hashingService.hash(correctPassword)).thenReturn("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");

        PasswordCheckingService passwordCheckingService = new PasswordCheckingService(hashingService, loginRepositoryService);
        PlayerInformationTO result = passwordCheckingService.checkPasswordAndReturnInformationTO(user, correctPassword);

        assertEquals(playerInformationTO, result);
    }
}
