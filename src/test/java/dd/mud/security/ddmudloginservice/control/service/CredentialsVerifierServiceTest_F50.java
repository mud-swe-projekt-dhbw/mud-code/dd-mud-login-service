package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.boundary.model.SignInTO;
import dd.mud.security.ddmudloginservice.entity.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CredentialsVerifierServiceTest_F50 {

    @InjectMocks
    CredentialsVerifierService credentialsVerifierService;

    @Mock
    LoginRepositoryService loginRepositoryService;
    @Mock
    MailService mailService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getUserFromMailTest() {
        final String mail = "tom@187.de";
        SignInTO signInTo = new SignInTO(mail, "1234");
        User user = new User(1L, mail, "tom", null, null, false, 0L);

        when(loginRepositoryService.getUserByMail(mail)).thenReturn(user);
        when(mailService.isMailValid(mail)).thenReturn(true);

        User actual = credentialsVerifierService.getUser(signInTo);
        assertEquals(user, actual);

        verify(loginRepositoryService, times(0)).getUserFromUsername(anyString());
        verify(loginRepositoryService, times(1)).getUserByMail(mail);
    }

    @Test
    public void getUserByUsernameTest() {
        final String mail = "tom@187.de";
        final String username = "tom";
        SignInTO signInTo = new SignInTO(username, "1234");
        User user = new User(1L, mail, username, null, null, false, 0L);

        when(loginRepositoryService.getUserFromUsername(username)).thenReturn(user);
        when(mailService.isMailValid(username)).thenReturn(false);

        User actual = credentialsVerifierService.getUser(signInTo);
        assertEquals(user, actual);

        verify(loginRepositoryService, times(1)).getUserFromUsername(username);
        verify(loginRepositoryService, times(0)).getUserByMail(anyString());
    }
}