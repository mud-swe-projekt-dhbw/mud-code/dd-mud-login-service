package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.boundary.model.Mail;
import dd.mud.security.ddmudloginservice.control.exceptions.MailServerConnectionFailedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CheckMailConnectionException {

    @Test
    public void checkIfMailServerConnectionFailedExceptionIsThrown(){
        MailService mailService = new MailService();
        Mail mail = new Mail("", "", "", "", "");

        Assertions.assertThrows(MailServerConnectionFailedException.class, () -> {
            mailService.sendMail(mail);
        });
    }

}
