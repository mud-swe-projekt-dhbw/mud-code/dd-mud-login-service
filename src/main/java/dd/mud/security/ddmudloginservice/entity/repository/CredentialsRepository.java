package dd.mud.security.ddmudloginservice.entity.repository;

import dd.mud.security.ddmudloginservice.entity.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface CredentialsRepository extends CrudRepository<User, Long> {

    // Save method is in CrudRepository
    Optional<User> findByUsername (String username);
    Optional<User> findByMail (String mail);
    Optional<User> findByMailHash (String mailHash);
    boolean existsByUsername (String username);
    boolean existsByMail (String mail);
}
