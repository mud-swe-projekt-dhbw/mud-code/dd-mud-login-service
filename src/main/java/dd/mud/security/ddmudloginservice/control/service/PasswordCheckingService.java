package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.boundary.model.PlayerInformationTO;
import dd.mud.security.ddmudloginservice.control.exceptions.PasswordIsWrongException;
import dd.mud.security.ddmudloginservice.entity.model.User;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PasswordCheckingService {

    private final HashingService hashingService;
    private final LoginRepositoryService loginRepositoryService;

    /**
     * Checks if the hashed input is equal to the password hash in the database.
     * @param user The user-object included in DB
     * @param enteredPassword The password entered by user.
     * @return The player information if the validation was successful.
     */
    public PlayerInformationTO checkPasswordAndReturnInformationTO (@NonNull User user, @NonNull String enteredPassword) {

        if (!isHashEqual(user, enteredPassword)) {
            throw new PasswordIsWrongException();
        }
        return PlayerInformationTO.builder()
                .playerId(user.getId())
                .userName(user.getUsername())
                .rank(user.getRank())
                .build();
    }

    private boolean isHashEqual(@NonNull User usernamePasswordHash, @NonNull String enteredPassword) {
        String enteredPasswordHash = hashingService.hash(enteredPassword);
        return usernamePasswordHash.getPasswordHash().equals(enteredPasswordHash);
    }
}
