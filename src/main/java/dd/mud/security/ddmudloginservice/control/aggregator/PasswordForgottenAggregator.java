package dd.mud.security.ddmudloginservice.control.aggregator;

import dd.mud.security.ddmudloginservice.boundary.model.Mail;
import dd.mud.security.ddmudloginservice.control.exceptions.UserNotFoundException;
import dd.mud.security.ddmudloginservice.control.service.HashingService;
import dd.mud.security.ddmudloginservice.control.service.LoginRepositoryService;
import dd.mud.security.ddmudloginservice.control.service.MailService;
import dd.mud.security.ddmudloginservice.entity.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PasswordForgottenAggregator {

    private final MailService mailService;
    private final LoginRepositoryService loginRepositoryService;
    private final HashingService hashingService;

    /**
     * Takes the mail and sends a mail with the link to reset the password.
     * @param mailAddress The receiverAddress.
     */
    public void sendPasswordForgottenMail (String mailAddress) {
        User user = loginRepositoryService.getUserByMail(mailAddress);
        if (user != null) {
            Mail mail = mailService.buildPasswordForgottenMail(mailAddress, user.getMailHash());
            mailService.loginToMailServer();
            mailService.sendMail(mail);
            mailService.logoutFromMailServer();
        }
        else {
            throw new UserNotFoundException();
        }
    }

    /**
     * Resets the password
     * @param mailHash The mail Hash to identify the user.
     * @param password The new password to persist.
     */
    public void resetPassword (String mailHash, String password) {
        User user = loginRepositoryService.getUserByMailHash(mailHash);
        String passwordHash = hashingService.hash(password);
        user.setPasswordHash(passwordHash);
        loginRepositoryService.saveUser(user);
    }
}
