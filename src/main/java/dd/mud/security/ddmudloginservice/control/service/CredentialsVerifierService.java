package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.boundary.model.SignInTO;
import dd.mud.security.ddmudloginservice.control.exceptions.RegistrationNotCompletedException;
import dd.mud.security.ddmudloginservice.entity.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CredentialsVerifierService {

    private final LoginRepositoryService loginRepositoryService;
    private final MailService mailService;

    /**
     * Get User-object from DB using entered mail/username
     * @param signInTO The information entered by user.
     * @return -
     */
    public User getUser(SignInTO signInTO) {
        User user;
        if(isEmail(signInTO)){
            user = loginRepositoryService.getUserByMail(signInTO.getMailOrUsername());
        }
        else {
            user = loginRepositoryService.getUserFromUsername(signInTO.getMailOrUsername());
        }
        return user;
    }

    private boolean isEmail(SignInTO signInTO) {
        return mailService.isMailValid(signInTO.getMailOrUsername());
    }

    /**
     * Throws Exception, when registration is not completed yet
     * @param user The User-Object containing the DB-infos
     */
    public void verifyRegistrationStatus(User user) {
        if(!user.isRegistrationStatus()){
            throw new RegistrationNotCompletedException("Die Registrierung ist nicht abgeschlossen!");
        }
    }
}
