package dd.mud.security.ddmudloginservice.control.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserDataAlreadyUsedException extends RuntimeException {
}
