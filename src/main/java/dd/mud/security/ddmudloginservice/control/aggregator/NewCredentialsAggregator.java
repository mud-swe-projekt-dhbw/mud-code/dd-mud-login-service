package dd.mud.security.ddmudloginservice.control.aggregator;

import dd.mud.security.ddmudloginservice.boundary.model.CredentialsTO;
import dd.mud.security.ddmudloginservice.boundary.model.Mail;
import dd.mud.security.ddmudloginservice.control.exceptions.EmailNotValidException;
import dd.mud.security.ddmudloginservice.control.exceptions.UserDataAlreadyUsedException;
import dd.mud.security.ddmudloginservice.control.service.LoginRepositoryService;
import dd.mud.security.ddmudloginservice.control.service.HashingService;
import dd.mud.security.ddmudloginservice.control.service.MailService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class NewCredentialsAggregator {

    private final HashingService hashingService;
    private final LoginRepositoryService loginRepositoryService;
    private final MailService mailService;

    /**
     * Hashes the password using the HashingService
     * and saves it together with the username in the database.
     * @param credentialsTO A UsernamePasswordTO object to be hashed and persisted.
     */
    public void createNewCredentials (CredentialsTO credentialsTO) {
        //removes leading and trailing spaces
        credentialsTO.setUsername(credentialsTO.getUsername().trim());
        credentialsTO.setMail(credentialsTO.getMail().trim());

        // Check for redundant user
        boolean availableUserName = !loginRepositoryService.usernameExists(credentialsTO.getUsername());
        //Check for semantic correctness
        boolean syntacticallyValidUserName = loginRepositoryService.isSyntaxValid(credentialsTO.getUsername());

        // Hashing
        MailService mailService = new MailService();
        String passwordHash = hashingService.hash(credentialsTO.getPassword());
        String mailHash = hashingService.hash(credentialsTO.getMail());

        // Persists the user data in the database.
        if (availableUserName && syntacticallyValidUserName && validateMail(credentialsTO.getMail())) {
            loginRepositoryService.saveCredentials(credentialsTO.getUsername(),
                    passwordHash,
                    credentialsTO.getMail(),
                    mailHash);

            // Build confirmation mail
            Mail mail = mailService.buildConfirmationMail(credentialsTO.getMail(), mailHash);

            // Connect to server and send the mail
            mailService.loginToMailServer();
            mailService.sendMail(mail);
            mailService.logoutFromMailServer();
        }
        else{
            throw new UserDataAlreadyUsedException();
        }
    }

    /**
     * Sets the registration status according to the mailHash.
     * @param mailHash The mailHash to identify the account.
     */
    public void confirmRegistration (String mailHash) {
        loginRepositoryService.setRegistrationStatusByMailHash(mailHash);
    }

    /**
     * Checks whether there is already an account using the given mail and checks the
     * the address syntactically.
     * @param mail The mail address to verify
     * @return True when the mail is valid according to the described criteria, otherwise false.
     */
    private boolean validateMail (String mail) {
        boolean mailAddressExists = loginRepositoryService.mailExists(mail);

        if(!mailService.isMailValid(mail)){
            throw new EmailNotValidException();
        }
        return !mailAddressExists;
    }
}
