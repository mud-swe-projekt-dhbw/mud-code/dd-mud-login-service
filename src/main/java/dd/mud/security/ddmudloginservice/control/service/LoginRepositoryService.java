package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.control.ForbiddenSymbolsUsername;
import dd.mud.security.ddmudloginservice.control.exceptions.UserNotFoundException;
import dd.mud.security.ddmudloginservice.control.exceptions.UsernameSyntacticallyInvalidException;
import dd.mud.security.ddmudloginservice.entity.model.User;
import dd.mud.security.ddmudloginservice.entity.repository.CredentialsRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class LoginRepositoryService {

    private final CredentialsRepository usernamePasswordRepository;

    /**
     * Saves the username with the password in the database.
     * @param username The username to save.
     * @param passwordHash The password passwordHash to save.
     */
    public void saveCredentials(@NonNull String username, @NonNull String passwordHash, @NonNull String mail, @NonNull String mailHash) {
        usernamePasswordRepository.save(User.builder()
                .username(username)
                .passwordHash(passwordHash)
                .mail(mail)
                .mailHash(mailHash)
                .rank(0L)
                .build());
    }

    /**
     * Retrieves the a user found by its username.
     * @param username The username to search with.
     * @return A usernamePasswordHash object according to the username.
     */
    public User getUserFromUsername (String username) {
        return usernamePasswordRepository.findByUsername(username).orElseThrow(UserNotFoundException::new);
    }

    /**
     * Checks if a user already exists.
     * @param username The username to check with.
     * @return Returns true if it exists, otherwise false.
     */
    public boolean usernameExists (String username) {
        return usernamePasswordRepository.existsByUsername(username);
    }

    /**
     * Checks if a user exists by mail.
     * @param mail The mail to check with
     * @return true when user exists.
     */
    public boolean mailExists (String mail) {
        return usernamePasswordRepository.existsByMail(mail);
    }

    /**
     * Sets the registration status.
     * @param mailHash The mail to select the account.
     */
    public void setRegistrationStatusByMailHash (String mailHash) {
       User user = this.usernamePasswordRepository.findByMailHash(mailHash).orElseThrow(UserNotFoundException::new);
       user.setRegistrationStatus(true);
       this.usernamePasswordRepository.save(user);
    }

    /**
     * Finds a user in the database by his mail.
     * @param mail The mail of the user to search for.
     * @return The user corresponding to the given mail.
     */
    public User getUserByMail (String mail) {
        return usernamePasswordRepository.findByMail(mail).orElseThrow(UserNotFoundException::new);
    }

    /**
     * Finds the user in the database by his mail.
     * @param mailHash The mailHash of the user to search for.
     * @return The user corresponding to the given mailHash.
     */
    public User getUserByMailHash (String mailHash) {
        return usernamePasswordRepository.findByMailHash(mailHash).orElseThrow(UserNotFoundException::new);
    }

    /**
     * Persists the given user
     * @param user The user to persist.
     */
    public void saveUser (User user) {
        usernamePasswordRepository.save(user);
    }

    /**
     * Checks syntax of entered username
     * @param username
     * @return true if username is syntactically correct, else throw usernameSyntacticallyInvalidException()
     */
    public boolean isSyntaxValid(String username) {

        List<String> forbiddenSymbols = Arrays.asList(ForbiddenSymbolsUsername.FORBIDDEN_AT,
                ForbiddenSymbolsUsername.FORBIDDEN_COLON, ForbiddenSymbolsUsername.FORBIDDEN_WHITESPACE);

        for (String forbiddenSymbol : forbiddenSymbols) {
            if(username.contains(forbiddenSymbol)){
                throw new UsernameSyntacticallyInvalidException();
            }
        }

        List<String> forbiddenUsernames = Arrays.asList(
                ForbiddenSymbolsUsername.FORBIDDEN_USERNAME0, ForbiddenSymbolsUsername.FORBIDDEN_USERNAME1,
                ForbiddenSymbolsUsername.FORBIDDEN_USERNAME2, ForbiddenSymbolsUsername.FORBIDDEN_USERNAME3,
                ForbiddenSymbolsUsername.FORBIDDEN_EMPTY_USERNAME);

        for (String forbiddenUsername : forbiddenUsernames) {
            if(username.equals(forbiddenUsername)){
                throw new UsernameSyntacticallyInvalidException();
            }
        }

        return true;
    }
}
