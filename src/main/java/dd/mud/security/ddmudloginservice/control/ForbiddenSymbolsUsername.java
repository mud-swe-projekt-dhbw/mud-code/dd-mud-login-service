package dd.mud.security.ddmudloginservice.control;


public final class ForbiddenSymbolsUsername {
    //forbidden Symbols
    public static final String FORBIDDEN_AT = "@";
    public static final String FORBIDDEN_WHITESPACE = " ";
    public static final String FORBIDDEN_COLON = ":";

    //forbidden Usernames
    public static final String FORBIDDEN_USERNAME0 = "DM";
    public static final String FORBIDDEN_USERNAME1 = "dm";
    public static final String FORBIDDEN_USERNAME2 = "Dm";
    public static final String FORBIDDEN_USERNAME3 = "dM";
    public static final String FORBIDDEN_EMPTY_USERNAME = "";
}
