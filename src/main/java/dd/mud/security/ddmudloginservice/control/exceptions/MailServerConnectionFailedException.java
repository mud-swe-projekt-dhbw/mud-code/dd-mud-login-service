package dd.mud.security.ddmudloginservice.control.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MailServerConnectionFailedException extends RuntimeException {
}
