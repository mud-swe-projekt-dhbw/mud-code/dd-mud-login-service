package dd.mud.security.ddmudloginservice.control.service;

import dd.mud.security.ddmudloginservice.boundary.model.Mail;
import dd.mud.security.ddmudloginservice.control.exceptions.MailServerConnectionFailedException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MailService {
    /**
     * Represents a mailsession.
     */
    protected Session mailsession;

    /**
     * Checks if the given mail address is valid.
     * @param mail The mail address as string.
     */
    public boolean isMailValid(String mail) {
        return EmailValidator.getInstance().isValid(mail);
    }

    /**
     * Performs a login to the google smpt-server using mail credentials.
     */
    public void loginToMailServer () {
        // Set properties of the server
        Properties properties = configureMailServer();

        // Login to the server
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("dhbwdragons@gmail.com", "mudserver");
            }
        };

        this.mailsession = Session.getDefaultInstance(properties, authenticator);
    }

    /**
     * Takes the mail in the parameter and send it.
     * @param mail An object of type Mail
     */
    public void sendMail(Mail mail) {
        if (mailsession == null) {
            throw new MailServerConnectionFailedException();
        }

        try {
            // Configure Mail
            MimeMessage message = new MimeMessage(mailsession);
            message.addHeader("Content-type", "text/html; charset=UTF-8");
            message.addHeader("format", "flowed");
            message.addHeader("Content-Transfer-Encoding", "8bit");
            message.setFrom(new InternetAddress(mail.getSenderAdress(),false));
            message.setReplyTo(InternetAddress.parse(mail.getSenderAdress(), false));
            message.setSubject(mail.getSubject(), "UTF-8");
            message.setText(mail.getMessage(), "UTF-8");
            message.setSentDate(new Date());
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getReceiverAdress(), false));

            // Send Mail
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs a logout from the smtp-server.
     */
    public void logoutFromMailServer() {
        this.mailsession = null;
    }

    /**
     * Builds a confirmation mail and returns a Mail object
     * @param receiverAddress The receiver address
     * @param mailHash The hash of the reveiver address to build the verification link.
     * @return An object of type Mail
     */
    public Mail buildConfirmationMail (String receiverAddress, String mailHash) {
        String confirmationLink = "https://dd01.dv-xlab.de:49152/login/confirm-registration?mailHash=" + mailHash;
        return Mail.builder()
                .senderAdress("dhbwdragons@gmail.com")
                .senderName("MUD DHBW Dragons")
                .receiverAdress(receiverAddress)
                .subject("Confirmation Link")
                .message("Welcome to our MUD Server " + confirmationLink)
                .build();
    }

    /**
     * Builds the password forgotten mail.
     * @param receiverAddress The address of the receiver.
     * @param mailHash The hash of the mail to build the link for the REST-call
     * @return An object of type mail to send.
     */
    public Mail buildPasswordForgottenMail (String receiverAddress, String mailHash) {
        String passwordForgottenLink = "https://dd01.dv-xlab.de:8080/password-forgotten/passwordForgotten.html?mailHash=" + mailHash;
        return Mail.builder()
                .senderAdress("dhbwdragons@gmail.com")
                .senderName("MUD DHBW Dragons")
                .receiverAdress(receiverAddress)
                .subject("Reset password")
                .message("Here you can reset your password " + passwordForgottenLink)
                .build();
    }

    /**
     * Set necessary properties to connect to the google SMTP-server
     * @return An object of type Properties containing the server configuration.
     */
    private Properties configureMailServer() {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.socketFactory.port", "587");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.EnableSSL.enable","true");
        properties.put("mail.smtp.ssl.checkserveridentity", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", "587");
        return properties;
    }
}
