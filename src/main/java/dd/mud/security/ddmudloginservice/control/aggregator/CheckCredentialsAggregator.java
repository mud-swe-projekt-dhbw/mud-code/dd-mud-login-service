package dd.mud.security.ddmudloginservice.control.aggregator;

import dd.mud.security.ddmudloginservice.boundary.model.PlayerInformationTO;
import dd.mud.security.ddmudloginservice.boundary.model.SignInTO;
import dd.mud.security.ddmudloginservice.control.service.CredentialsVerifierService;
import dd.mud.security.ddmudloginservice.control.service.LoginRepositoryService;
import dd.mud.security.ddmudloginservice.control.service.PasswordCheckingService;
import dd.mud.security.ddmudloginservice.entity.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class CheckCredentialsAggregator {

    private final CredentialsVerifierService credentialsVerifierService;
    private final LoginRepositoryService loginRepositoryService;
    private final PasswordCheckingService passwordCheckingService;

    /**
     * Checks credentials of user:
     * 1: Get User-object from DB using entered mail/username
     * 2: Is registration completed?
     * 3: Is entered password correct?
     * @param signInTO The information entered by user.
     * @return PlayerInformationTO if conditions 2 and 3 are true -> else throw Exception
     */
    public PlayerInformationTO checkCredentials (SignInTO signInTO) {

        User user = credentialsVerifierService.getUser(signInTO);
        credentialsVerifierService.verifyRegistrationStatus(user);

        return passwordCheckingService.checkPasswordAndReturnInformationTO(user, signInTO.getPassword());
    }
}
