package dd.mud.security.ddmudloginservice.control.exceptions;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@NoArgsConstructor
@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Wrong Password!")
public class PasswordIsWrongException extends RuntimeException {
}
