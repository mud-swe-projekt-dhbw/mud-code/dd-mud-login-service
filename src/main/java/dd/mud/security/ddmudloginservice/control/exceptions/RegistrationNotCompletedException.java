package dd.mud.security.ddmudloginservice.control.exceptions;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@NoArgsConstructor
@ResponseStatus(code = HttpStatus.LOCKED, reason = "Registration process uncompleted!")
public class RegistrationNotCompletedException extends RuntimeException {
    public RegistrationNotCompletedException(String message) {
        super(message);
    }
}
