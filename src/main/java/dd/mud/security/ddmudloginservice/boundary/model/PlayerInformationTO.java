package dd.mud.security.ddmudloginservice.boundary.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Data
@Builder
public class PlayerInformationTO {

    private Long playerId;

    private String userName;

    private Long rank;
}
