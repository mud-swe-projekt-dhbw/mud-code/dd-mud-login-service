package dd.mud.security.ddmudloginservice.boundary;

import dd.mud.security.ddmudloginservice.boundary.model.*;
import dd.mud.security.ddmudloginservice.control.aggregator.CheckCredentialsAggregator;
import dd.mud.security.ddmudloginservice.control.aggregator.NewCredentialsAggregator;
import dd.mud.security.ddmudloginservice.control.aggregator.PasswordForgottenAggregator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/login")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginApi {

    private final NewCredentialsAggregator newCredentialsAggregator;
    private final CheckCredentialsAggregator checkCredentialsAggregator;
    private final PasswordForgottenAggregator passwordForgottenAggregator;

    /**
     * REST-call for registration of a new user
     * @param credentialsTO The submitted credentials
     * @return HTTP-Answer correct
     */
    @PostMapping(value = "/sign-up")
    public ResponseEntity<Void> signUpUser(@RequestBody CredentialsTO credentialsTO) {
        // delegate to control
        newCredentialsAggregator.createNewCredentials(credentialsTO);
        // response in good case, other see exception handler
        return ResponseEntity.status(201).build();
    }

    /**
     * REST-call to confirm the registration.
     * @param mailHash The hash of the email-address to search in the database.
     * @return A positive HTTP-answer and the confirmation message of the registration process.
     */
    @GetMapping(value = "/confirm-registration")
    public ResponseEntity<Void> confirmRegistration(@RequestParam String mailHash) {
        String confirmationMessage = "You have been registered successfully. You can close this tab now";
        newCredentialsAggregator.confirmRegistration(mailHash);
        return new ResponseEntity(confirmationMessage, HttpStatus.OK);
    }

    /**
     * Checks whether the submitted credentials are unique --> persists them if they are unique.
     * @param signInTO The submitted credentials.
     * @return A positive HTTP-answer.
     */
    @PostMapping(value = "/check-credentials")
    public ResponseEntity<PlayerInformationTO> checkCredentials (@RequestBody SignInTO signInTO) {

        // delegate to control
        PlayerInformationTO playerInformationTO = checkCredentialsAggregator.checkCredentials(signInTO);

        // send response in good case, other see exception handler
        return ResponseEntity.ok().body(playerInformationTO);
    }

    /**
     * Initiates the password forgotten service so that the password can be restored.
     * @param passwordRequestTO The mail to identify the account.
     * @return A positive HTTP-answer.
     */
    @PutMapping(value = "/password-forgotten")
    public ResponseEntity<Void> passwordForgotten (@RequestBody PasswordRequestTO passwordRequestTO) {
        passwordForgottenAggregator.sendPasswordForgottenMail(passwordRequestTO.getMail());
        return ResponseEntity.ok().build();
    }

    /**
     * Resets the password of the account corresponding to the given mailHash
     * @param mailHash The mailHash to identify the account
     * @param passwordTO The new password in a PasswortTO instance
     * @return HTTP-status 200
     */
    @PutMapping(value = "/reset-password")
    public ResponseEntity<Void> resetPassword (@RequestParam String mailHash, @RequestBody PasswordTO passwordTO) {
        passwordForgottenAggregator.resetPassword(mailHash, passwordTO.getPassword());
        return ResponseEntity.ok().build();
    }
}
