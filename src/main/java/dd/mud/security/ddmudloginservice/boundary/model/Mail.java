package dd.mud.security.ddmudloginservice.boundary.model;

import lombok.*;
import org.hibernate.annotations.common.reflection.XProperty;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Mail {
    private String senderAdress;
    private String senderName;
    private String receiverAdress;
    private String subject;
    private String message;
}
