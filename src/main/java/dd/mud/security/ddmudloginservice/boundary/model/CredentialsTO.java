package dd.mud.security.ddmudloginservice.boundary.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Data
public class CredentialsTO {

    private String mail;
    private String username;
    private String password;
}
