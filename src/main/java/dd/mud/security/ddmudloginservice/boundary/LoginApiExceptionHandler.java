package dd.mud.security.ddmudloginservice.boundary;

import dd.mud.security.ddmudloginservice.boundary.model.ErrorMessage;
import dd.mud.security.ddmudloginservice.control.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class LoginApiExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Void> handleUnexpectedException(Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UserDataAlreadyUsedException.class)
    public ResponseEntity<Void> handleUnexpectedException(UserDataAlreadyUsedException e) {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PasswordIsWrongException.class)
    public ResponseEntity<Void> handleUnexpectedException(PasswordIsWrongException e) {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(EmailNotValidException.class)
    public ResponseEntity<Void> handleUnexpectedException(EmailNotValidException e) {
        return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(UsernameSyntacticallyInvalidException.class)
    public ResponseEntity<Void> handleUnexpectedException(UsernameSyntacticallyInvalidException e) {
        return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(RegistrationNotCompletedException.class)
    public ResponseEntity<ErrorMessage> handleUnexpectedException(RegistrationNotCompletedException e) {
        return ResponseEntity.status(HttpStatus.LOCKED).body(ErrorMessage.builder().error(e.getMessage()).build());
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorMessage> handleUnexpectedException(UserNotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorMessage.builder().error(e.getMessage()).build());
    }
}
